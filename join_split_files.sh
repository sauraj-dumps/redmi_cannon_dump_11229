#!/bin/bash

cat vendor/data-app/SmartHome/SmartHome.apk.* 2>/dev/null >> vendor/data-app/SmartHome/SmartHome.apk
rm -f vendor/data-app/SmartHome/SmartHome.apk.* 2>/dev/null
cat system/system/apex/com.android.vndk.current.apex.* 2>/dev/null >> system/system/apex/com.android.vndk.current.apex
rm -f system/system/apex/com.android.vndk.current.apex.* 2>/dev/null
cat system/system/apex/com.android.art.release.apex.* 2>/dev/null >> system/system/apex/com.android.art.release.apex
rm -f system/system/apex/com.android.art.release.apex.* 2>/dev/null
cat system/system/app/VoiceAssist/VoiceAssist.apk.* 2>/dev/null >> system/system/app/VoiceAssist/VoiceAssist.apk
rm -f system/system/app/VoiceAssist/VoiceAssist.apk.* 2>/dev/null
cat system/system/app/SogouInput/SogouInput.apk.* 2>/dev/null >> system/system/app/SogouInput/SogouInput.apk
rm -f system/system/app/SogouInput/SogouInput.apk.* 2>/dev/null
cat system/system/priv-app/MIUISecurityCenter/MIUISecurityCenter.apk.* 2>/dev/null >> system/system/priv-app/MIUISecurityCenter/MIUISecurityCenter.apk
rm -f system/system/priv-app/MIUISecurityCenter/MIUISecurityCenter.apk.* 2>/dev/null
cat system/system/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null >> system/system/priv-app/MiuiCamera/MiuiCamera.apk
rm -f system/system/priv-app/MiuiCamera/MiuiCamera.apk.* 2>/dev/null
cat system/system/priv-app/MIUIMusic/MIUIMusic.apk.* 2>/dev/null >> system/system/priv-app/MIUIMusic/MIUIMusic.apk
rm -f system/system/priv-app/MIUIMusic/MIUIMusic.apk.* 2>/dev/null
cat system/system/priv-app/MIUIVideo/MIUIVideo.apk.* 2>/dev/null >> system/system/priv-app/MIUIVideo/MIUIVideo.apk
rm -f system/system/priv-app/MIUIVideo/MIUIVideo.apk.* 2>/dev/null
cat system/system/priv-app/Browser/Browser.apk.* 2>/dev/null >> system/system/priv-app/Browser/Browser.apk
rm -f system/system/priv-app/Browser/Browser.apk.* 2>/dev/null
cat system/system/priv-app/MIUIGallery/MIUIGallery.apk.* 2>/dev/null >> system/system/priv-app/MIUIGallery/MIUIGallery.apk
rm -f system/system/priv-app/MIUIGallery/MIUIGallery.apk.* 2>/dev/null
cat system/system/data-app/com.baidu.searchbox_9/com.baidu.searchbox_9.apk.* 2>/dev/null >> system/system/data-app/com.baidu.searchbox_9/com.baidu.searchbox_9.apk
rm -f system/system/data-app/com.baidu.searchbox_9/com.baidu.searchbox_9.apk.* 2>/dev/null
cat system/system/data-app/com.ss.android.ugc.aweme_15/com.ss.android.ugc.aweme_15.apk.* 2>/dev/null >> system/system/data-app/com.ss.android.ugc.aweme_15/com.ss.android.ugc.aweme_15.apk
rm -f system/system/data-app/com.ss.android.ugc.aweme_15/com.ss.android.ugc.aweme_15.apk.* 2>/dev/null
cat system/system/data-app/Youpin/Youpin.apk.* 2>/dev/null >> system/system/data-app/Youpin/Youpin.apk
rm -f system/system/data-app/Youpin/Youpin.apk.* 2>/dev/null
cat system/system/data-app/MIUIYoupin/MIUIYoupin.apk.* 2>/dev/null >> system/system/data-app/MIUIYoupin/MIUIYoupin.apk
rm -f system/system/data-app/MIUIYoupin/MIUIYoupin.apk.* 2>/dev/null
cat system/system/data-app/MiShop/MiShop.apk.* 2>/dev/null >> system/system/data-app/MiShop/MiShop.apk
rm -f system/system/data-app/MiShop/MiShop.apk.* 2>/dev/null
cat system/system/data-app/MIUIEmail/MIUIEmail.apk.* 2>/dev/null >> system/system/data-app/MIUIEmail/MIUIEmail.apk
rm -f system/system/data-app/MIUIEmail/MIUIEmail.apk.* 2>/dev/null
cat system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system/system/system_ext/priv-app/Settings/Settings.apk
rm -f system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat boot.img.* 2>/dev/null >> boot.img
rm -f boot.img.* 2>/dev/null
cat product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> product/app/WebViewGoogle/WebViewGoogle.apk
rm -f product/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> product/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f product/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> product/priv-app/GmsCore/GmsCore.apk
rm -f product/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
